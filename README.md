# Xiaomi Redmi Note 8 Pro ( codename begonia)
![](https://rubencarneiro.com/wp-content/themes/yootheme/cache/redmi-note8-pro-eb634943.png)

Xiaomi Redmi Note 8 Pro (begonia)
==========================================

The Redmi Note 8 Pro (codenamed _"begonia"_) is a high-end, mid-range smartphone from Xiaomi.
It was released in September 2019.

| Basic                   | Spec Sheet                                                                                                                     |
| -----------------------:|:------------------------------------------------------------------------------------------------------------------------------ |
| CPU                     | Octa-core                                                                                                                      |
| Chipset                 | Mediatek Helio G90T                                                                                                            |
| GPU                     | Mali-G76 MC4                                                                                                                   |
| Memory                  | 6/8 GB RAM                                                                                                                     |
| Shipped Android Version | 9.0                                                                                                                            |
| Storage                 | 64/128/256 GB                                                                                                                  |
| Battery                 | Non-removable Li-Po 4500 mAh battery                                                                                           |
| Display                 | 1080 x 2340 pixels, 19.5:9 ratio (~395 ppi density)                                                                            |
| Camera (Back)(Main)     | 64 MP, f/1.9, 26mm (wide), 1/1.7", 0.8µm, PDAF                                                                                |
| Camera (Front)          | 20 MP, f/2.0, 0.9µm
# What works so far?

- [X] LXC
- [X] Boot
- [X] Bluetooth
- [X] Camera Fotos and Video Recording
- [X] GPS
- [X] NFC
- [X] Audio works
- [X] Audio Routing ( still need a fix to loudspeker but now audio dont get stuck on earpiece )
- [X] Bluetooth Audio
- [X] Waydroid with gaaps
- [X] MTP
- [X] ADB
- [X] SSH
- [X] Online charge
- [ ] Offline Charge
- [X] Widi is still needs testing
- [X] Bluetooth
- [X] Wifi
- [X] Calls
- [X] Mobile Data 2G/3G/4G (LTE)
- [X] Ofono Wont autoselect data we need tp select manual to get signal
- [X] SDCard
- [X] OTG Works
- [X] Camera Flash
- [X] Manual Brightness Works
- [X] Switching between cameras
- [X] Hardware video playback
- [X] Rotation
- [X] Proximity sensor
- [X] KVM Virtualization
- [X] GPU
- [X] Lightsensor is working as Light hal is reporting working
- [X] Proximity sensor
- [X] Automatic brightness
- [X] Torch

# For internal use these link should not be shared public

- Issues should be reported on https://gitlab.sagetea.ai/xfone/core/haliumports/a10/xiaomi-redmi-note-8-pro/xiaomi-begonia/issues
